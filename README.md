# Installation

 1. Clone the docker repository https://gitlab.com/Mohannad_Elemary/news-system-docker
 2. in the `src` folder, clone both backend and frontend repositories https://gitlab.com/Mohannad_Elemary/news-system-backend and https://gitlab.com/Mohannad_Elemary/news-system-frontend
 3. Rename the backend project to `news-backend` and the frontend project to `news-frontend`
 4. Copy `.env.example` from the backend and frontend projects and rename them to `.env`
 5. Run `docker-compose up --build`
 6. Enter the backend container `docker exec -it "php" sh` and run `sudo chmod -R 777 /storage` and `php artisan migrate`
 7. In the backend container run `php artisan  passport:client  --password` as well to create the auth client.
 8. A client ID and secret will be outputed, put them in your `.env` in the frontend application
 9. Restart the containers
 10. It will output a link to visit the frontend. Use it to open the frontend on the browser.
 11. Backend link: `http://localhost/`
 12. PHPMyAdmin Link: `http://localhost:8085/`, the host is `mysql`, username `root` and no password

# Postman Link
https://documenter.getpostman.com/view/8868758/2s935uGLTC
It contains a documentation for the APIs with examples for each request call. 

# Flow

```mermaid
graph LR
A[Daily Crons] -- dispatch queues messages to sync from integrations --> B((sync articles queue consumer))
B --> C(Newyork API)
B --> D(The Guardian API)
B --> E(The News API)
B -- Dispatch messages with the fetched articles to store them --> F(save articles queue consumer)
F --> G(Database)
```

 1. We have a cron the runs everyday and sync the new news publiched in that day to our database
 2. That cron iterates over our available external news integration and dispatch messages to `sync-articles-queue` queue to request them and fetch the new news.
 3. The `sync-articles-queue` consumer starts consuming those messages and fetch the articles from the integration. The consumer start breaking those data as chunks and send each chunk in a message to the `save-articles-queue`
 4. The `save-articles-queue` consumer start recieving those messages, and save the authors, categories, souces, and articles data to our database.

Note: We are using `redis` as a queueing medium in our queues. But the best solution is to use `SQS`as redis is an in-memory storage and having a lot of messages could lead to a memory leak (not scalable).

# Scalability
We took take about scalability in our implementation by the following:

 1. Moving those heavy operations into queues (syncing data). So, it can handle much more operations. Also, we didn't depend on communicating with the integrations each time the user requests the articles as this leads to throttling problems when having an icrease in customer's numbers so the scalability will improve.
 2. we are executing those queues consumers in a separated containers in docker. This give us the ability to deploy them on a separated instances and handle their load a lone and not affecting the other parts of the system. Also, it will help us to auto-scale them easily whever we have high-load.

# Security
We took take about security in our implementation by the following:

 1. Sending customer's access tokens in http-only cookies and not storing them on local storage for example. This will prevent any attemp to do sessions hijacking or XSS attacks if anyone tries to steal those tokens from the customer's broswer via an injected JS code.
 2. We are validating any parameters that are sent to the server in the requests
 3. We are performing pagination in all of our listing APIs to prevent any attempt to overload the server with requesting a huge amount of resources at a time.

# Performance
We took take about performance in our implementation by the following:

 1. Applying the appropriate indices on the database for fast retrieving. Also, we applied full-text index on the text columns to ensure the users are able to seach though them very fast.
 2. Reducing the number of queries by doing relations eager-loading, and doing bulk inserts for the synced articles instead of doing a single insert for each on of them

# Tests
Tests are written to cover the possible scenarios in our features. And to make the development process much safer as we go. Also, the integrations are mocked and used in the tests.

# Clean code
 SOLID priciples, polymorphism (in the integrations), PSR standards, PHPdocs and datatypes are specified in the code to make it clean as possible.
